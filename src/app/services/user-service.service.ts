import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { ILogin } from '../modules/users/interface/login.interface';

//Le costanti vanno sopra a tutto perchè devono stare fuori dalla classe
//e non devono interferire con gli Injetable
//Questa costante racchiude in essa il token
//TOKEN DELL?AUTNTICAZIONE DELLA CHIAVE DELLO STORAGE, viene chiamata così per convenzione
const STORAGE_KEY_AUTH_TOKEN = 'auth-token';



@Injectable({
  providedIn: 'root'
})

export class UserServiceService {
//Come prima cosa vado a richiamarmi l'HTTPClient, che vado a mettere nel costruttore
  //l'HTTPClient mi serve per prendermi i dati dalla API
  constructor(private _http:HttpClient) {
    console.log('user service funziona')
   }
   //quando lavro con un api esterna di login ad esempio, vado a vedermi direttamente
   //la documentazione dell'API

   //Perciò come prima cos ami creo una funzione di LOGIN
   public login( email:string , password:string){
     //faccio il return di "this._http", perchè è quello che sta in questa pagina
     //che contiene l'HTTPClient. 
     //Gli passerò poi il metodo "post", perchè devo mandargli qualcosa all'API
     //Il metodo post, mi richiede di passargli l'INTERFACCIA di ciò che gli serve
     //L'interfaccia mi restituisce il token che ci deve dare per il login
     //Il token sarà sempre una stringa.
     //Oltre all'interfaccia , gli devo passare l'indirizzo dell'Api o un'altro indirizzo del server
    //e poi l'oggetto che gli devo mandare
     return this._http.post<ILogin>('https://reqres.in/api/login',{
            //gli passo i parametri che ho detto prima
            email, 
            password
     },{
       //ora gli passiamo l'observable perchè le chiamate sono 
       //asicrone e quindi necessitano degli Observable per prenderti il body
       //gli dico all'observable di osservare "response", perchè sta ossservando la risposta
       //Perciò , noi sappiamo che lui di default si prende il body, quindi noi gli diremo 
       //di osservare response perchè così lui si prende tutta la risposta per intero
       //che ci serve per capire se abbiamo avuto un messaggio di errore o meno
       //e se non abbiamo errori possiamo quindi prenderci ciò che ci serve
       observe: 'response'
     })
     //applico pipe perchè è uno dei meotodi degli observable
     //Nel pipe, richiamo il metodo map degli observable , perchè voglio applicare
     //una funzione a ciascun elemento
     .pipe( map( response => {
       //nell'if gli dico che se la risposta è giusta, sarà ".ok", che è un metodo
       //delle chiamate in HTTPClient
       if(response.ok){
        //Ottenuto quindi questo Token, devo metterlo da qualche parte
        //in questo caso lo metteremo nel session storage
        //Gli dico al sessionStorage di applicare il metodo ".setItem" che mi crea
        //una coppia "chiave/valore", perciò al di fuori di questa chiamata mi creerò una constamnte
        //dove andrò a dichiarare il token( ancora vuoto)
        //nelle tonde quindi andrò a fare il PAIRING (sto accoppiando) la costante (STORAGE_KEY_AUTH_TOKEN)
        //con il token stesso (response.body.token)
        //gli do il " ? " prima del token, perchè così in caso non odvesse torvarlo
        //non mi mandarebbe in panne l'applicativo. (RICORD L'ESEMPIO DI ALBERTO NELLA PAGINA DELL'API con la RESPONSE DEL TOKEN)
        sessionStorage.setItem(STORAGE_KEY_AUTH_TOKEN, response.body?.token as string)
        return true;
       }
       return false;
     }))
   }

   //FINITA LA CHIAMATE NELLA COSTANTE NON HO ANCORA COMUNQUE CONCLUSO
   
   //La prima dichiarazione di funzione sarà una funzione che permette all'Interceptor di vedere se il token esiste
   public hasToken ():boolean{
     //perciò gli vado a dire di ritornami in un booleano (perchè la risposta è true o false)
     //l'Interceptror va a guardare nel sessionStorage se l'item c'è con ".getItem" 
     //i punti esclamativi sono il bangoperator
     return !!sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN);
   }

  //La seconda dichiarazione funzione invece andrà a PRENDERE lo storage key del token 
  public getToken ():string{
    //questa volta vado a dirgli di trovarmi la stringa del token
    return sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN) as string;
  }

}
