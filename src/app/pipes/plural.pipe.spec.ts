import { PluralPipe } from './plural.pipe';

//E2E (enden to end -> ?? )
//INTEGRATION TEST (mi aiuto con il backend a testatre determinate cose??)
//UNIT TEST: (è una classe da testare, quindi come in questoe esempio)

describe('PluralPipe', () => {

  it('create an instance', () => {
    const pipe = new PluralPipe();
    expect(pipe).toBeTruthy();
  });

  //creiamo noi un caso di test, per ciò che ci serve
  it ('trasform car into cars', () => {
    //mi crei l'instanza della pipe
    const pipe = new PluralPipe();
    //io mi aspetto che invocando "trasform" e passandoglio 'car'
    //e che sia uguale a 'cars'. Perchè l'esecuzione di "pipe.transform('car')"
    //mi deve tirare fuori "cars", perciò "cars" tiratomi fuori da esso ,
    //sarà uguale (.toEqual) al "cars" dell'it.
    //Altrimenti la mia pipe non sarà "true"
    expect(pipe.transform('car')).toEqual('cars');
  });

  it ('trasform they into them', () =>{
    const pipe = new PluralPipe();
    expect (pipe.transform('they')).toEqual('them');
  });

  it ('trasform fe into ves', () => {
    const pipe = new PluralPipe();
    expect (pipe.transform('fe')).toEqual('ves');
  });

});
