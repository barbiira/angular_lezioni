import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plural'
})
export class PluralPipe implements PipeTransform {

  //value: unknown (o string o number ecc) --> la stringa dell'informazione dove applicare la pipe
  // (ad esempio birthday nella documentazione)
  // args --> valore opzionale da applicare
  transform(value: string, ...args: unknown[]): unknown {
    if (value === 'they'){
      return 'them';
    }
    const vesRegex= /(f|fe)$/;
   if (vesRegex.test(value)) {
     //il metodo "replace" delle stringhe  ci aiuta a dire in questo caso che
     //se trovi una regex sostituiscila con la stringa che ti dico. (quindi dx viene sostituito da sx)
     return value.replace(vesRegex,'ves');
   }

   const esRegex = /(s|ss|sh|ch|x|z)$/;
   if (esRegex.test(value)){
     //questa volta usiamo il metodo "concat" di una stringa
     //così da aggiungere quella stringa alla regex
    return value.concat('es');
   }

   //infine , prendiamo tutti gli altri valori stringa che devono divenatre plurali
   //e gli concateniamo solo "s".
   return value.concat('s');


    return null;
  }

}
