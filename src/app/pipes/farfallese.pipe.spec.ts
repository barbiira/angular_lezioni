import { FarfallesePipe } from './farfallese.pipe';

describe('FarfallesePipe', () => {
  it('create an instance', () => {
    const pipe = new FarfallesePipe();
    expect(pipe).toBeTruthy();
  });

  it ('trasform ciao to cifiafaofo', () =>{
    const pipe = new FarfallesePipe();
    expect (pipe.transform('ciao')).toEqual('cifiafaofo');
  });

});
