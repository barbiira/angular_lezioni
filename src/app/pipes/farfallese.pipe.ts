import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'farfallese'
})
export class FarfallesePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {

  const regexA = /[aeiou]+/;
  if (regexA.test(value)){
    return value.replace(regexA , match => `${match}f${match}`);
  }
    return null;
  }

}
