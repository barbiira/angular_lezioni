
export class Hero {
     constructor(
        public name : string,
        public pv : number,
        public damage : number,
        public pic: string
    ) {
        //
     }
}

