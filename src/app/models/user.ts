//lo scrivo sinfolare perchè è un modello unico, di un singolo utente
export class User{
    constructor(
        //gli passo tutti i parametri che mi servono
        public id: number,
        public email: string,
        public password: string
    ){
        //
    }
}
