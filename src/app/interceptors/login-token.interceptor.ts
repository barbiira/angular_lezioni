import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../modules/users/service/user.service';

@Injectable()
export class LoginTokenInterceptor implements HttpInterceptor {
//nel costruttore mi vado a iniettare lo UserService che contiene 
//tutto ciò che mi serve riguardo all'interceptor
  constructor(private _userService: UserService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
   //creo una costante che contiene il clone della richiesta
    const req = request.clone();

  //Contorllo se la chiamata ha effetettivamente preso il token
  if(this._userService.hasToken()){
    
    //magia mistica...FIDATE , deve essere sempre così per colpa del backend 
    //Bearer significa "portatore"
    req.headers.append('Authorization', `Bearer ${this._userService.getToken()}`)
  }
    //mi faccio il ritorno della richiesta
    return next.handle(req);
  }
}
