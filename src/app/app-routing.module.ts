import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArenaComponent } from './components/arena/arena.component';
import { CounterComponent } from './components/counter/counter.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

//Percorso di navigazione: oggetti che creano un associazione fra un componente
//e un path (percorso, tipo href).
const routes: Routes = [{
  component: ArenaComponent,
  path: 'arena'
},{
  //me lo carica dopo, questa funzione che vuole loadchildren
  //
  //import mi restituisce una promise
  //f è il parametro formale che si carica tutto
  loadChildren: () => import ('./modules/invoices/invoices.module').then(f => f.InvoicesModule ),
  path: 'invoices'
},{
  component: CounterComponent,
  path:'counter'
},{
  //ti da errore perchè non hai creato il componente "home" e "notFound"
  component: HomeComponent,
  path: ''
}, {
  //va messo alla fine perchè angular legge "top-down"
  component: NotFoundComponent,
  path: '**' //doppio wildecard, significa tutto, quindi mi dà tutto "not found"
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
