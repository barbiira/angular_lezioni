import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForbiddenDirective } from './directives/forbidden.directive';



@NgModule({
  declarations: [
    ForbiddenDirective
  ],
  exports:[
    ForbiddenDirective
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
