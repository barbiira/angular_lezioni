//qui mi esporto delle funzioni di validazione
import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

//Questa func si prenderà una o più stringhe non ammesse, e se quel campo in input, nel fom
//contiene una o più stringhe non ammesse il campo non sarà valido.
//Per passargli più di una striga o parametri usiamo lo spread operato " ... "
//e ci agglomera tutti i parametri attuali in unico parametro formale, per questo li mettiamo dentor un [array]
//il ValidatorFn si riferisce alla funzione interna e è un interfaccia.
//
//Appunti Andrea:
// Fn esterna
//   - Raccoglie dati utili ai fini della validazione
//   - Restituisce la funzione di validazione interna,
//     quella utilizzata di fatto per validare il campo del form
export const forbiddenValidator  = ( ...values: string []) : ValidatorFn => {
    //la funzione esterna (qui sopra) restuità quella interna(qua sotto dopo return) che a sua 
    //volta restituisce o un  ValidationError o Null
    // La funzione interna che viene restituita da quella esterna
    // Questa verrà usata da Angular per eseguire la validazione sul campo in input / gruppo
    //
    // Quando viene invocata da Angular, le viene passato il Control
    // istanziato sull'elemento del Form
    return (c: AbstractControl): ValidationErrors | null => {
        //questa sarà la funzione che Angular utilizzerà per la validazione del form
        //itero su tutti i values, prendendomi il singolo value per vedere quando ho l'errore
        for (const value of values){
            // Se quanto digitato dall'utente (c.value) coincide
            // con uno degli elementi nell'array (una delle parole non ammesse)restituisco un errore
            if (value === c.value){
                return{
                    //gli errori sono degli oggetti
                    //La chiave dell'oggetto dell'orrere la chiamiamo il nome dell'errore come la funzione
                    // La chiave da usare nell'oggetto di errore
                    // dovrebbe chiamarsi come la funzione di validazione
                    //
                    // Questa chiave è l'elemento che viene cercato
                    // con il metodo "hasError"
                    forbidden : true
                };
            }
        }
      
         // Nel caso in cui non si debba restituire un errore,
        // Angular impone la restituzione del valore null
        return null;
    };
};
