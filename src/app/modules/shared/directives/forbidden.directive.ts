import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { __values } from 'tslib';
import { forbiddenValidator } from '../validator/string.validator';

@Directive({
  //Per rendere una direttiva, una direttiva di validazione dovrò aggiure i providers
  //perchè le direttive di validazione vengono introdotte all'interno della DI
  providers:[{
    //Quando ad un token coincidono più istanze parliamo di "multiProvider"
    multi:true,
    //richiamo prima il mio token
    //è tutto maiuscolo per ricordarci che è statico
    provide: NG_VALIDATORS,
    //UseExistim è un istanza che va a ignettarsi su due token diversi
    //esso quindi mi permette di usare la stessa istanza per due token.
    //Lo uso per evitare di replicare ogni volta l'istanza di questa direttiva
    useExisting: ForbiddenDirective
  }],
  selector: '[appForbidden]'
})

//Le direttiva di validazione deve implementare l'interfaccia "validator"
//Essa ci impone di implemetare un metodo che prende il nome di "validate"
export class ForbiddenDirective implements Validator {

  //Mi creo un @Input per andare a portare il valore al template, dove richiamo 
  //il selettore ('appForbidden) e gli associo un parametro di ti tipo "array di stringhe"
  @Input ('appForbidden') forbidden !: string[];
  
  constructor() { }

  //il cuore della validazione lo fa "validate"
  public validate (control: AbstractControl): ValidationErrors | null{
    // fnINterna è la funzione di validazione interna
    //Nelle stringhe aggiungeremo le parole che l'utente non può usare
    //  const fnINterna = forbiddenValidator ('admin' , 'error');
    //  return fnInterna(control);
    //Possiamo scrivere questa cosa anche in maniera più concisa:
    //return forbiddenValidator ('boh' , 'bah')(control);
    //ho inserito ( ...this.forbidden) per richiamarmi l'array di stringhe.
   return forbiddenValidator (...this.forbidden)(control);

  }

}
