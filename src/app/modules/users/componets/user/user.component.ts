import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/user';
import { UserService } from '../../service/user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {

  public user!: User;


  constructor( private _route: ActivatedRoute, private _userService: UserService) { }
  

  ngOnInit(): void {
    //RAGIONAMENTO : prelevo l'id + chiamata al backend
    //param e query params sono DEPRECATE perchè delle vecchie versioni di "ANGULAR"
    //Bisogna usare quelli con "Map" finale
    //Questa notazione è un Observable che emetterà degli oggetti , quindi ci tocca fare un subscribe.
    //E' un observable , perchè essendo Angular sviluppato con rxjs, ci costringe a fare tutto in asincrono
    //Anche se questo Observable fa una sola chiamata e non servirebbe una funziona asincrona.
    //dentro le () ci prendiamo l'elemento che "emette" con un parametro p
    //L'elemento param map ha 3 metodi
    //get --> parametro di una stringa che voglio recuperare.
    //getAll --> mi da un array con tutti i dati
    //has --> vuole una stringa, però ci dice solo se quell'elemtno esiste o no (restituisce un booleano)
    //keys --> un array di stringhe con solo chiavi e non valori
    //il console.log mi restituisce una stringa, perchè ci riferiamo al percorso nell'url
    // this._route.paramMap.subscribe( p => console.log(p.get('id'))); 
    //
      //mi aggancio con ".pipe" al paramMap, che collega i due "stream" (le due linee temporali)
      this._route.paramMap.pipe(
        //prendo p che è la mappa, e solo la parte che mi interessa, che è il parametro id
        //e gli dico IO che è una stringa. Perciò non è più un Observable ora, perchè il valore
        //grazie al ".get" me lo da in sincrono
        map(p => p.get('id')as string),
      //Il risultato della funzione (Il valore dell'id) lo prende sempre l'operatore successivo, perciò id finisce come 
      //parametro attuale di "switchMap", che funziona come l'operatore map.
    
      //Quindi, quando il risultato della funzione è avvolto in un "Observable", non uso "Map" ma "switchMap"
      //Con SM se prima ci sta un'altro SM un risultato con un Observable, esso mi deregistra il risultato prima. 
        switchMap(id => this._userService.get(+id)) //perciò uso il SM perchè il risultato di dx è un Observable
        ).subscribe(user => this.user = user);  
  }

}

//!!!!RICORDA!!!!
//if direttiva strutturale che si applica sui selettori HTMl
//OPERATORE = funzioni costruite sugli observable, sono dentro ".pipe" che ci permette di elenecare uno o più operatori
//Quindi SwitchMap è un operatore dentro a ".pipe"