import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

public user: User [] = [
  // new User ( 1, 'uno@email' , 'uno'),
  // new User ( 2, 'due@email' , 'due'),
  // new User ( 3, 'tre@email' , 'tre')
]

//richiamo qui la coppia Token/Dipendeza
//la rendiamo privata , così da non farla modificare a nessuno.
//Il concetto di ACCESS MODIFY (quindi: privato/protetto/pubblico) serve solo agli svilupatori in questo caso

constructor(private _userService: UserService) {
 }

public handleDelete (user : User) : void {
  this.user.splice(
    this.user.indexOf(user),
    1
  )
}
  ngOnInit(): void {
    //lo invoco perchè voglio che mi restituisca un observable
    this._userService.list().subscribe(users => this.user = users);
    
  }

}
