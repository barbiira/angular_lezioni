import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //ci facciamo una prorpeità per ogni campo in Input
  //Queste proprietà accoglieranno tutte le informazioni del campo in input
  public email : string = '';
  public password : string = '';

  constructor() { }

  //dentro le () mi vado a passare il parametro dell'NgForm così me lo posso tirare dietro
  public handleSubmit(form : NgForm):void{
    //vado a vedere se effettivamente sono cambiati i valori dentro gli input
    //uscirà una stringa vuota in console se io non scrivo nulla nei campi in input
    //console.log(this.email, this.password)
    
    //faccio un if per dirmi che se il form non è valido , "esci".
    if (form.invalid){
      //invalid mi serve per bloccare "delle chiamate", quindi si bloccherà (sarà true) ogni volta che 
      //un "required" di un campo in input non verrà rispettato.
      return;
    }
    console.log(this.email, this.password)
  }

  ngOnInit(): void {
  }

}
