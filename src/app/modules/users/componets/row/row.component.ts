import { Component,Input, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: '[app-row]',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnInit {
  @Input() public user !: User;

  @Output() delete: EventEmitter <User> = new EventEmitter ();
  
    public handleRemove (): void {
      this.delete.emit(this.user);
    }

  constructor() { }

  ngOnInit(): void {
  }

}
