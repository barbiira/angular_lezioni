import { invalid } from '@angular/compiler/src/render3/view/util';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { forbiddenValidator } from 'src/app/modules/shared/validator/string.validator';

@Component({
  selector: 'app-rdf-login',
  templateUrl: './rdf-login.component.html',
  styleUrls: ['./rdf-login.component.scss']
})
export class RdfLoginComponent implements OnInit {

  //creo una proprietà pubblica di tipo form group e vado subito ad istanziare
  public form: FormGroup = new FormGroup({
    //La prima informazione che prende è quella iniziale, perciò se noi
      //gli passiamo una stringa vuota, leggerà quella
      //questo può essere utile quando lo stato iniziale di un campo in input in un form
      //deve essere vuoto perchè IO devo passargli dei dati (es "modifica indirizzodi Amazon"  //ES Differenza tra "MODIFICA FORM" - "POPOLA FORM")
    'email': new FormControl( '', [
      Validators.required,
      Validators.email,
      forbiddenValidator('admin@eng.it')
      //potremmo anche creare una funzione con la regola di validazione
      //Ma sarebbe inutile perchè non possiamo riutilizzarla
    ]),
    'password': new FormControl('', [
      Validators.required,
      Validators.minLength(8)
    ])
  });

  constructor() { }

  //I getter sono dei metodoi che all'interno delle classi sono delle prorpietà fittizzie
  //soluzione semplice:
  //public get email() : FormControl{ return this.form.get('email')} 
  //e sul template avremmo : <p *ngIf="email?.hasError('required')">Campo obbligatorio</p>
  //soluzione complicata:
  public get email() : FormControl{
    //.get mi restiuisce un "AbstractControl" o null
    //quindi con form, mi prendo o il FormControl o il FormGroup
    //perciò con 'email' mi sto tirando giù il formControl
    //as --> (il casting è una conversione di tipo)è un "type assertion" ci permette di 
    // (come il !) fermare un controllo di Ts, e di dirgli "stai tranquillo, fidati di me"
    //Se io però gli passo un valore che è sbagliato (quindi non il FormControl) mi darà comunque errore ma dopo
    //Perciò io gli sto assegnado un FormControl,mi dichiara quello che già è.
      return this.form.get('email') as FormControl;

      //Perciò se ci rendiamo conto che all'interno del template invochiamo elementi
      //che sono esclusivi del FromControl, usiamo il metodo con "as"
      //altrimenti quello con il "?" nel template
  }

  public handleSubmit(): void{
    if (this.form.invalid){
      return;
    }
    //c'è il punto interrogativo prima del punto ti permette di ignore la mancanza di un valore
    //quindi se e solo esiste esiste guarda il valore di ".value" altrimenti mi restitusci null
    console.log(this.form.get('email')?.value);
  }

  ngOnInit(): void {
  }

}
