import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private _userService: UserService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const req = request.clone();

    //controlliamo se esiste
    if (this._userService.hasToken()){
      //....speigazione troppo veloce di andrea,piango
      req.headers.append('Authorizaion', `Bearer ${this._userService.getToken()}`)
    }
    //passa al prossimo intercept,
    return next.handle(req);
  }
}


//Abbiamo realizzato un interceptor, ch intercetta la richiesta e verifica il token (?) 
//verrà rieseguito ad ogni tipologia di richiesta
