import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import nuovi:
import { UsersRoutingModule } from './users-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptor/token.interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './componets/login/login.component';
import { RdfLoginComponent } from './componets/rdf-login/rdf-login.component';
import { RowComponent } from './componets/row/row.component';
import { TableComponent } from './componets/table/table.component';
import { UserComponent } from './componets/user/user.component';


@NgModule({
  //richiamo tutto ciò che ho spostato qui, che erano le stesse cose che avevo in "app.module.ts"
  //rimeto tutti i componenti che ho spostato nella declaration
  declarations: [
    LoginComponent,
    RdfLoginComponent,
    RowComponent,
    TableComponent,
    UserComponent
  ],
  //rimetto tutti gli import che avevo, in questa import
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    SharedModule
  ],
  //cre nuovamente i providers che avevo
  providers:[{
      //multiprovider = più sottotoken
      multi:true,
      //ABBIAMO UN TOKEN E TANTI INTERCEPTROS
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor
  }]
})

export class UsersModule {
  //con questo costruttore andremmo ad analizzare quando questo modulo viene caricato
  constructor(){
    //per vedere il console.log, devo ricordarmi importare la classe "UsersModule" nell'array import di "app.module.ts"
    //in console, vedrò che il messagio in cosole, sarà la prima voce di tutto, perchè è
    //caricato in EAGER LOADING.
    console.log('UsersModule loaded!')
  }
 }
