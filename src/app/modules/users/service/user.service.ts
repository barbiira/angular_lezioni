import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//Rxjs deve essere importato
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { IUser } from '../interface/user.interface';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import { ILogin } from '../interface/login.interface';


//il valore non viene calcolato a run-time, perciò la scrivo grande e dashcase, e le viene asseganto un valore letterale
const STORAGE_KEY_AUTH_TOKEN = 'auth-token'


@Injectable({
  providedIn: 'root'
})

export class UserService {
  //Creo un metodo per non dover fare piu "newUser" tante volte
  //Objects -> to model
  //Prende in Input un interfaccia e mi va a restituire una nuova instanza di User
  private _objToModel (obj : IUser): User {
    return new User (obj.id, obj.email, obj.name);
  }

  //creo un istanza privata
  constructor(private _http: HttpClient) {
    //questo console.log funzionerà solamente se io vado a richiamare la coppia
    //chiave/valore o meglio TOKEN/DIPENDEZA userSercie: UserService
    //dentro un costruttore (esempio quello della tabella/user).
    //IOC = invercrion of control . Parte dal basso verso l'alto, quindi bottom->up.
    console.log('user service creato!')
  }

  //Il metodo list, senza return lancia soltano senza mai emettere informazioni
  //quindi dobbiamo fare un return e anche un Observable, per via del metodo get.
  public list(): Observable<User[]> {
    //mi aggancerò alla proprietà privata "_http"
    //sceglerò il metodo "get" perchè devo andarmi a prelevare dei valori.
    //Il primo valore che dobbiamo passargli è un "url"
    //ma non così com in questo esempio, perchè è una zozzata, visto che la prima parte è fissa
    //questa chiamata mi restituisce un array di elementi, grazie alle []
    //IUser è un interefaccia (simile alla classse, ma senza metodi e proprietà, descrivono solo come si chiamo
    //le proprietà e di che tipo devono essere).
    return this._http.get<IUser[]>(`${environment.api.url}/users`).pipe(
      //con .pipe mi vado a fare un stream parallelo
      //in questo stream pararallelo l'emissione che io avrò mi restituirà l'array user.
      //ora faccio il map di rxjs che mi restituisce un array, e o trasformo in un array di model
      map(users => users.map(user => this._objToModel(user)))
    );
  }

  //Dobbiamo resituire l'obserable
  public get(id: number): Observable<User> {
    return this._http.get<IUser>(`${environment.api.url}/users/${id}`).pipe(
      //map inteso come operatore
      map(user => this._objToModel(user))
    );
  }

  public getToken(): string {
    return sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN) as string;
  }

  public hasToken():boolean {
    // con !! faccio il double-bang-operator (che è tipo una doppia negazione) 
    return !!sessionStorage.getItem(STORAGE_KEY_AUTH_TOKEN);
  }

  //serve per farmi fare il login su una piattaforma
  //stiamo cheidendo al backend un token nella sua banca dati
    //post mi da una risposta che devo prototimizzarmi, perciò gli passiamo un Interfaccia
    //dove assegnamo al token un valore
  public login (email:string , password:string){
    return this._http.post<ILogin>( 'https://reqres.in/api/login' , {
      email,
      password
    },{
      //errore 401 nel login?
      //Angular di default osserva il body ma vogliamo tutto.
      //Lo facciamo con lo stesso oggetto che usiamo per chiamare gli header
      // o con : observe: 'body', oppure con:
      observe:'response' //ci da informazioni su tutta la risposta
    }).pipe(
      map(resposnse => {
        if (resposnse.ok){
          //setItem() lavora con una chiave + valore.
        sessionStorage.setItem(STORAGE_KEY_AUTH_TOKEN, resposnse.body?.token as string);
        //restituisco se "ho fatto il login bene? true"
        return true;
        }
        return false;
      })
    );
  }

}


//IL session storage , perde i dati solo alla chiusura del sito.
//Il local storage, non ha scadenza, a meno che non la cancelli da codice (localstorage.remove) o 
//l'utente entri nell impostazioni del browser e cancella la history.
//Usiamo session storage se ci serve che il token scada in tempi brevi.
//Usiamo local storage se ci serve che il token non scada in tempi brevi e si ricordi dell'accesso che è stato fatto.

//Le applicaizoni server-side (quelle dove si svuota la pagina): lato client, ci arrivano
//dei cuookie, fra cui quello dove abbiamo l'autenticazione. Questi cookie  (ad ogni richiesta)  
//vengono automaticamente aggiunti alle richieste che si faranno in seguito.
//Le app per cerllulari che  non hanno un concetto di cookie saranno delle applicazioni "rest-side"

//Interfacce rest --> assesnza di sessione, perciò usiamo il token di autenticazione,
//ogni richiesta deve avere al suo interno (negli header) un token di autenticazione,
//ogni token appartiene all'utente con id associato e ha una validità di tot tempo, dopodichè verrà eleiminato

//JWT, decido io come gestire il token dell'id che mi viene passato.
//OAUT è una strategia x cui ci facciamo autenticare dai server di Google o altro e in base a quelle info che
//ci passa Google/Facebook ecc noi ci salviamo l'id di riferimento nel nostro backend.

//Esiste anche un refersh token, cosìcchè quando viene refersciato , cioè che l'utente non è più
//autenticato, e grazie al refresh gli chiedo di rinnovare la sessione di quell'utente (ma per un max di tot volte)
