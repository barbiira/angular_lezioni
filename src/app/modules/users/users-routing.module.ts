import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//components
import { LoginComponent } from './componets/login/login.component';
import { RdfLoginComponent } from './componets/rdf-login/rdf-login.component';
import { TableComponent } from './componets/table/table.component';
import { UserComponent } from './componets/user/user.component';
import { IsLoggedGuard } from './guards/is-logged.guard';

const routes: Routes = [{
    component: LoginComponent,
    path:'login'
  },{
    component:RdfLoginComponent,
    path:'login-rdf'
  },{
      //GUARD: possono essere più di una, quindi = array.
      //ad esempio se volessi controllare il tipo di abbonamento di un utente
    canActivate:[IsLoggedGuard],
    //me lo sposto sopra perchè è la parte statica (esempio :id/profile - profile/:id)
    children:[{
      //a questo componente abbiamo aggiunto un percorso secondario, che può essere varibile.
      component: UserComponent,
      //mi vado a prendere l'id della tabella utente
      path:'user/:id'
    }],
    component: TableComponent,
    path: 'users'
  }];

@NgModule({
  //il forChild serve per estendere la configurazione di un modulo
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
