import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  //Creiamo un constructor per potergli iniettare il servizio
  //Router per fare il redirect nella pagina di login
  //visto che , se non sei loggato non puoi visualizzare la tabella, grazie al "return false"
  constructor (private _router: Router){
    //
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      //Restituiamo il risultato con un Observable o una Promise:
      //Con Promise: fatta da me
      // return new Promise((resolve, reject) =>{
      //   setTimeout(() =>{
      //      resolve(this._router.navigate(['/login']))
      //     }, 2000);
      //    return false;
      // });

      //FATTA DA ANDREA:
      return new Promise((resolve, reject) =>{
          setTimeout(() =>{
            this._router.navigate(['/login'])
             return false;
            }, 2000);
        });
      //.navigate è asicrono
      // this._router.navigate(['/login']);
      //ho inserito la Guard nella root di user, se io qui mettessi quindi
      // "return false" la tabella, nonostante io la prema sulla navbar non me la apre
      //perchè non ho i permessi per accedervi, mentre con
      // "retorn true" mi mostra la tabella
    //return false
  }
}
