import { Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor( private _el: ElementRef <HTMLElement>) { 
    console.log('highlight directive');
  }

  private _color!: string;
  @Input('appHighlight') color !: string;

  //Esercizio risolto da me: public color : string = this._el.nativeElement.style.backgroundColor;

  //Mi creo un metodo
  //Nella quale metterò un messagio per vedere se l'ascoltatore viene ascolato.
  @HostListener('mouseover') 
  public handleMouseOver(): void{
      //console.log('mouse over');
      this._color = this._el.nativeElement.style.backgroundColor;
      this._el.nativeElement.style.backgroundColor = this.color;
  }

  @HostListener('mouseout')
  public handleMouseOut(): void {
    //console.log('mouseout');
    this._el.nativeElement.style.backgroundColor= this._color;
  }

}

