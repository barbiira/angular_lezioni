import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  //public num:number = 0; --> è la stessa cosa che scrivere: public num = 0;
  //@Input è il decoratore.@Input() public num = 0;
  @Input() public num: number = 0;
  //Scritta da Andrea, ma senza else e con il return , cosìchè da fare una chiamata in meno.
  //Con ":void" gli dico che non deve ritornare nulla.

  public hanldeCalc(isSum:boolean): void{
    if (isSum){
      this.num++;
    return;}
    this.num--;
  }
  //Singola
  //op:boolean perchè il valore che mi restiturià sarà per forza true/false
  //quindi o op===true -> aggiungi  , else op ===false -> sottrai.
  // public hanldeCalc(op:boolean){
  //   if (op === true){
  //     this.num++;
  //   } else {
  //     this.num--;
  //   }
  // }
  //Doppia 
  // public handleSum(){
  // this.title++;
  // };
  // public handleSot(){
  // this.title--;
  // };


  constructor() { }

  ngOnInit(): void {
  }

}
