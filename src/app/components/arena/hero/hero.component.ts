import { Component,Input, EventEmitter, OnInit, Output } from '@angular/core';
import { Hero } from '../../../models/hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
 
  @Input() public hero!: Hero;

  @Input() public color!: string;

  @Output() attack: EventEmitter <Hero> = new EventEmitter ();
  

    public handleClick (): void {
      this.attack.emit(this.hero);
    }

  constructor() { }

  ngOnInit(): void {
  }

}