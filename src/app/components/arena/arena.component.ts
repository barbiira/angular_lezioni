import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../../models/hero';

@Component({
  selector: 'app-arena',
  templateUrl: './arena.component.html',
  styleUrls: ['./arena.component.scss']
})
    
export class ArenaComponent implements OnInit {
  // public pg1 = new Hero ( 'Tony' , 20, 100, 'pic');
  // public pg2 = new Hero ( 'Cap' , 10, 100, 'pic');
  // public handleTony(damage : number){
  //   if (damage){
  //     this.pg2.pv -= this.pg2.damage ;
  //   } 
  //   return this.pg1.pv;
  // }
  
  // public handleCap(damage : number){
  //   if (damage){
  //     this.pg1.pv -= this.pg1.damage ;
  //   }
  //   return this.pg2.pv;
  // }

  

  public hero1 : Hero = new Hero ('Sonia', 100, 20, 'https://crispythoughts.files.wordpress.com/2014/06/hqdefault.jpg?w=640');
  public hero2 : Hero = new Hero ('Tonio Cartonio', 100, 10, 'https://virali.video/wp-content/uploads/2020/06/melevisioni.jpg');

  public handleAttack ( hero : Hero){
    if (this.hero1 === hero) {
      this.hero2.pv -= this.hero1.damage;
    } else {
      this.hero1.pv -= this.hero2.damage;
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

}


