import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-triagebutt',
  templateUrl: './triagebutt.component.html',
  styleUrls: ['./triagebutt.component.scss']
})
export class TriagebuttComponent implements OnInit {
  
  public c : string = "";
  public handleColor(color:string){
    this.c = color;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
