import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TriagebuttComponent } from './triagebutt.component';

describe('TriagebuttComponent', () => {
  let component: TriagebuttComponent;
  let fixture: ComponentFixture<TriagebuttComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TriagebuttComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TriagebuttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
