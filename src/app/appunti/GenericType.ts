//generic type
//con any passiamo qualsiasi tipo di datp, si pia qualsiasi cosa, ma non tiene traccia 
//del tipo di dato che  io gli sto passando.

// function fn (p:any) :any {
//     return p;
// }
// let n = fn (5); // questo non è corretto

//Sulla funzione devo definire il generic type fn(nomefunzione) <geneictype> (parametro)
function fn<T>(p:T): T {
    return p;
}
let n = fn(5);
n.toFixed() //n. mi da di seguito tutti i metodi e le proprietà ":number", se io mettissi una stringa
//n. mi suggerirebbe tutte le proprietà/metodi delle stringhe.
//Quindi il Generic Type ci permette di usare qualsiasi tupo di dato e ne tiene traccia, a differenza di any
//che gli passa un valore gnerico, punto, senza tenere nessuna traccia.


//Per passare più generic type :
// function fm<T , K>(p:T , s: K): T | K {
//     return p;
// }
// let a = fm(5 , '6');
//se scrivessi a. andrebbe ad ingrociare le proprietà, perciò sta cosa , per ora non si fa
//la andremmo a vedere più avanti.






