import { Component, Input } from '@angular/core'; //@angular/core è in nucleo principale del framework

//La funzione la importo e @component la invoco, perchè gli sto passando dei parametri
//E la funzione @component fa diventare le classi componenti
@Component({
  //Diveneterà il tag in HTML <app-root> che richiamerà il componente
  selector: 'app-root',
  //stringa con percorso che punta al file HTML 
  templateUrl: './app.component.html',
  //Foglio di stile del componente
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //di default la proprità in una classe (access modifyed) è pubblic , quindi aggiungiamo "pubblic" per renderle esplicito
  //title = 'ts-angular';
  //title è una proprietà di tipo stringa, perchè se il contenuto della variabile o proprietà 
  //è di tipo stringa anche la variabile a sua volta lo sarà.(TS è appunto tipizzato)
  //EX pubblic title:string = 'etl-ng' --> quindi omette la specifica " : string "(che è data-type), come fa con pubblic.
  //Se non metti nulla nell'assegnazione e non dichiari il data-type il valore sarà "any", ma comunque devi scrivere ":any"
  //alrimenti darà errore, ma anche scrivere ":any" è da evitare.
 public title = 'ts-angular';


//Creiamo il metodo che richiamiamo nell'html nell' "event binding"
 public handleClick(){
   alert('Hai fatto click');
 }

@Input() public nam: number = 5;
 constructor (){
   setTimeout(() => this.nam = 3, 5000);
 }

}

 

