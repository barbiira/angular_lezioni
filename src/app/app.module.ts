import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter/counter.component';
import { TriagebuttComponent } from './components/triagebutt/triagebutt.component';
import { ArenaComponent } from './components/arena/arena.component';
import { HeroComponent } from './components/arena/hero/hero.component';

import { HighlightDirective } from './directives/highlight.directive';

import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';



import { PluralPipe } from './pipes/plural.pipe';
import { FarfallesePipe } from './pipes/farfallese.pipe';
import { UsersModule } from './modules/users/users.module';
import { SharedModule } from './modules/shared/shared.module';

//Chiavi del decorator che trasmorma le classi in un module
@NgModule({
  //Dentro declaration vengono dichiarati i componenti, le direttive e i pipes
  declarations: [
    AppComponent,
    CounterComponent,
    TriagebuttComponent,
    ArenaComponent,
    HeroComponent,
 
    HighlightDirective,
    HomeComponent,
    NotFoundComponent,
   
    PluralPipe,
    FarfallesePipe
  ],
  //Dentro imports decliniamo i moduli da cui vogliamo attingere funzionalità
  //Ad esempio in Angular abbiamo 1 modulo x gestire i form, se il mio app modules deve utilizzare i form
  //qui ci metterò l'import per la gestione dei form
  imports: [
    //Il BrowserModules, importa a sua volta il commond module
    //serve a declianre tutti i moduli da cui attingere attività
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    UsersModule,
    SharedModule,
    //dobbiamo mettere per ultimo l'AppRoutingModule, perchè essendo statico
    //è l'unica parte che deve essere letta dopo, così tutte le cose vengono caricate
    AppRoutingModule,
  ],
  //Quando leggiamo providers (servizi kai) dentro verranno declinati tutti queli elementi (solitamente classi che per ..
  //convenzione vegono chiamate "servizi", essi grazie alla DIAI possono essere iniettati all'interno dell'applicazione)
  providers: [
    //prende un oggetto per ogni elemento che vogliamo registrare
    //provide: UserService, //Token
    //useClass: UserService, //dipendenza
    //Quindi abbiamo così creato la coppia "Token-Dipendenza"
    //TUTTO CIO SI PUO SEMPLIFICARE SCRIVENDO SOLAMENTE:
    //UserService,
    // {
    //   //multiprovider = più sottotoken
    //   multi:true,
    //   //ABBIAMO UN TOKEN E TANTI INTERCEPTROS
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: TokenInterceptor,
    // }
    //
    //LO ABBIAMO POSTATO TUTTO IN "users.module.ts"
  ],
  //bootstrap si trova solo nel modulo principale (NON è LA LIBRERIA CSS, am signica all'avvio = allacciarsi le scarpe)
  //Ci permette di indicare quali sono i componenti da caricare all'avvio dell'app 
  bootstrap: [AppComponent]
})
export class AppModule { }
